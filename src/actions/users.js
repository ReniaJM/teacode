export const ACTION_TYPES = {
    USER_FETCH: "USER_FETCH",
    USER_FETCH_SUCCESS: "USER_FETCH_SUCCESS",
    USER_FETCH_ERROR: "USER_FETCH_ERROR"
};

export function fetchUser(user) {
    return {
        type: ACTION_TYPES.USER_FETCH,
        user
    };
}

export function fetchUserSuccess(user) {
    return {
        type: ACTION_TYPES.USER_FETCH_SUCCESS,
        user
    };
}

export function fetchUserError() {
    return {
        type: ACTION_TYPES.USER_FETCH_ERROR
    };
}

export const getUsers = () => {
    return fetch('data/test.json'
    , {
        headers : { 
          'Content-Type': 'application/json',
          'Accept': 'application/json'
         }
      }
    )
      .then(response =>
        Promise.all([response, response.json()])
    )
};

export const fetchUsersToApp = () => {
    return dispatch => {
        dispatch(fetchUser());
        return getUsers().then(([response, json]) => {
            if (response.status === 200) {
                dispatch(fetchUserSuccess(json));
            } else {
                dispatch(fetchUserError());
            }
        });
    };
};
