import { createStore, applyMiddleware } from 'redux'
import reducer from '../reducer/mainReducer'
import thunk from "redux-thunk";


const logger = store => next => action => {
    let result = next(action);
    return result;
};

export default createStore(reducer, applyMiddleware(logger, thunk));
