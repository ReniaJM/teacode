import React from "react";
import UserContainer from "../../container/UserContainer";
import PropTypes from "prop-types";

const UsersList = ({user, first_name}) => {
    const textLower = first_name.toString().toLowerCase();

    const userWithFilter = user.filter(user =>
        user.first_name.toLowerCase().includes(textLower)
    );

    return (
        <div>
            {userWithFilter.length > 0 ? (
                userWithFilter.map(e => {
                    return <UserContainer key={e.id} {...e} />
                })
            ) : (
                <div>
                    <h2>
                        <span role="img" aria-label="magnifying glass tilted left">&#128269; </span> Oops, nothing found</h2>
                </div>
            )}

        </div>
    )

};

UsersList.propTypes = {
    user: PropTypes.array.isRequired,
    first_name: PropTypes.string.isRequired,
};


export default UsersList;

