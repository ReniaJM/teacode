import React from "react";
import PropTypes from 'prop-types';

const User = ({avatar, first_name, last_name}) => {
    return (
        <>
    <div className="list-group" >
        <div className="row g-0 list-group-item list-group-item-action">
            <div className="col-md-2">
                <img src={avatar} alt="..."/>
            </div>
            <div className="col-md-6">
                <div className="card-body">
                    <h5 className="card-title">{first_name}</h5>
                    <p className="card-text">{last_name}</p>
                    <div className="form-check col-md-4">
                        <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault"/>
                    </div>
                </div>
            </div>
         
        </div>
      
    </div>
 
  </>
    );
};

User.propTypes = {
    avatar: PropTypes.string.isRequired,
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
    email: PropTypes.string.isRequired,
  
};

export default User;


