import React, {useEffect, useState} from "react";
import UsersList from "../../components/UsersList/UsersList";
import PropTypes from "prop-types";
import "./home.css";

const HomePage = ({user,  isLoading, isError, fetchUsersToApp}) => {
    useEffect(() => {
        fetchUsersToApp();
    }, [fetchUsersToApp]);

    const [first_name, searchName] = useState('');
  

    const clearInput = () => {
        searchName('')
      
    };

    if (isError) {
        return <h2 className="info-error">Sorry, there's an error during fetching data </h2>;
    }

    if (isLoading) {
        return <div className="spinner"/>
    }

    return (
        <>
            <h2 className="text-center pt-3">Contacts</h2>
            <div className="input-group mb-3">
                <input type="text" className="form-control p-2"
                    placeholder="Search..."
                    id="first_name"
                    value={first_name}
                    onChange={e => searchName(e.target.value)}/>
                <div className="input-group-append">
                    <button className="btn btn-dark" type="button" onClick={clearInput}>Clear</button>
                </div>
            </div>

            <UsersList user={user} first_name={first_name} />
        </>
    );
};


HomePage.propTypes = {
    isLoading: PropTypes.bool.isRequired,
    isError: PropTypes.bool.isRequired,
    fetchUsersToApp: PropTypes.func.isRequired,
};


export default HomePage;

