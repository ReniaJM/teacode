import {ACTION_TYPES} from '../actions/users';

const initialState = {user: null, isLoading:true, isError: false};

const user = ( state = initialState, action) =>{
    switch (action.type) {   
        case ACTION_TYPES.USER_FETCH:
            return {...initialState};
        case ACTION_TYPES.USER_FETCH_SUCCESS:
            return {user: action.user, isLoading:false,isError: false };
        case ACTION_TYPES.USER_FETCH_ERROR:
            return {user: null, isLoading:false, isError: true };
         default:
             return state;        


    }

}

export default user;