import {connect} from "react-redux";

import HomePage from "../pages/Home/HomePage";

import {fetchUsersToApp} from "../actions/users";

const mapStateToProps = state => ({
    user: state.users.user,
    isLoading: state.users.isLoading,
    isError: state.users.isError
});

const mapDispatchToProps = {
    fetchUsersToApp: fetchUsersToApp
};

const HomePageContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(HomePage);

export default HomePageContainer;
