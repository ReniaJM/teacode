import {connect} from "react-redux";

import User from "../components/User/User";

const UserContainer = connect(
    null,
)(User);

export default UserContainer;
