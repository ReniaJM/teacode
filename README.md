# Project with React Redux  - API Fetch

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

View [live working app](https://gitlab.com/ReniaJM/teacode).


## Description

Example simple SPA site based on React and Redux that:

1. Fetch example contacts list from (https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json).
2. Render 1000 contacts.
3. Have input text field with possibility to filter displayed contact by `first_name`.


## Running

```
npm i
npm start
```
